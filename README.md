# The Vim Based Development Environment

## 1. Introduction

Welcome to the [(Space)Vim](https://spacevim.org/) based development environment which is containerized using Docker.

If you reach there this is because you are the maintainer or just curious to see how things are done behind the scene. However, if you are simply a developer that wants to use the image without the willing to deep dive into the technical details of the design, then you sould jump to the section [6. Using the images](#using-the-image).
 
Two kind of images are available: 
(1) the default for CPU machines, and 
(2) an NVIDIA CUDA based image for GPU machines.

## 2. Build the images

**WARNING:** 
Once again, you don't have to do that if you just want to use the image(s). Skip to the last section to see how to simply use the image(s).

**Prerequisites:**
The following actions are required to guarantee the success of the images building. 
The following binaries MUST be manually downloaded and copied in a directory named *tarballs* under the root directory:


```shell
$ tree tarballs/
tarballs/
├── conque.vim.tar.gz
├── dot_vim_bundle.tar.gz
├── go1.13.3.linux-amd64.tar.gz
└── nerd-fonts.tar.gz
```

<i>Note: Contact me to obtain the file **dot_vim_bundle.tar.gz** which contains the Vim plugins</i>

**Build the image(s): CPU or GPU**

```shell
$ cd scripts
$ sudo ./docker-build-vim-dev-lab[-gpu].sh
```

At the end of the execution of the image building script, the following manual is displayed: 

```
...
Successfully tagged vim-dev-lab-gpu:2.0.0
[INFO]
[INFO] Image vim-dev-lab-gpu:2.0.0 is successfully created. Done. 
[INFO] Cleaning the Docker registry by removing dangling images...
[INFO] Done.
[INFO]
[INFO]
[INFO] You can start a container as follows:
[INFO]
[INFO] $ sudo docker run --runtime=nvidia --rm -d -it
[INFO]        [--privileged]
[INFO]        [--name PROJECT_NAME]
[INFO]        [-u developer]
[INFO]        [-v HOST_PROJECT_DIR:CONTAINER_PROJECT_DIR]
[INFO]        [-e DISPLAY=$DISPLAY]
[INFO]        [-v /tmp/.X11-unix:/tmp/.X11-unix]
[INFO]        vim-dev-lab-gpu:2.0.0
[INFO]
[INFO] In the previous command, all [] are optional.
[INFO]
[INFO] Access to the container through a terminal using the command:
[INFO] $ sudo docker exec -it PROJECT_NAME /bin/bash
[INFO]
[INFO] Stop the container as follows:
[INFO] $ sudo docker stop PROJECT_NAME
[INFO]
[INFO] Or in foreground with:
[INFO] $ sudo docker run --runtime=nvidia --rm -it vim-dev-lab-gpu:2.0.0
[INFO]
```
 

Check the images:

```shell
$ docker images | grep vim-dev-lab
vim-dev-lab-gpu                     2.0.0                    c95a1c69043b        9 days ago          4.27GB
vim-dev-lab                         2.0.0                    9a965dc5f7f8        9 days ago          11.3GB
```

### 2.1. Post-install actions

The installation steps of many useful packages are hard to script through a Dockerfile. The reasons are multiple and beyond the scocpe of this document. For this reasons, one can perform the following steps to manually install or configre additional packages.

#### 2.1.1. The Remarkable Markdown Editor

Start a container in background (bg) mode: 

```shell
$ sudo ./scripts/run-bg-vim-dev-lab-cde.sh
```

The container is run in bg using the name *vim-dev-lab*. 

Attach a terminal to the running container, and execute the following commands (that will fail):

```shell
$ sudo docker exec -it vim-dev-lab /bin/bash
developer@02484f98c51e:/$ cd
developer@02484f98c51e:~$ wget -c https://remarkableapp.github.io/files/remarkable_1.87_all.deb
developer@02484f98c51e:~$ sudo apt-get update 
developer@02484f98c51e:~$ sudo dpkg -i remarkable_1.87_all.deb 
... 
Errors were encountered while processing: remarkable
``` 
As you noticed, the installation failed, and It is "normal"; so let us fix now: 

```shell 
developer@02484f98c51e:~$ sudo apt-get install -f
... // very very long. Once done, install again the package
developer@02484f98c51e:~$ sudo dpkg -i remarkable_1.87_all.deb 
... // install is now OK! You can clean
developer@02484f98c51e:~$ rm remarkable_1.87_all.deb 
```

Test it by running the GUI:
```shell
developer@02484f98c51e:~$ remarkable 
```

#### 2.1.2. Other packages

One should take the opportunity to install missing package. For instance, at the time we write these lines, the *Mozilla Firefox* browser is not installed during the image construction. It can be interesting to install it now...
 
#### 2.1.3. Commit the image updates

Once all the supplemental packages are installed or other post-install actions performed ; it is time to persist the changes in the images: 

First, always check the version of the running image: 

```shell
$ sudo docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
647568e5f890        vim-dev-lab:2.0.0   "sh -c 'sudo /etc/in…"   20 hours ago        Up 20 hours         22/tcp              vim-dev-lab
```

Second, commit the changes to a new version: 

```shell
$ sudo docker commit vim-dev-lab vim-dev-lab:2.0.1
```

Here, the next version of the image is *vim-dev-lab:2.0.1*.



## 3. Push to docker hub

**Prerequisites:**

+ You need to have an account on docker hub (e.g. username = rejux)
+ You need to create a repository a set of tags on the docker hub (e.g. rejux/vim-dev-lab)

step 1: tag the (just created) image before pushing using the same name as your remote repository:

```shell
$ sudo docker tag vim-dev-lab:latest rejux/vim-dev-lab:2.0.0
```

step 2: login to the hub using your docker client and push

```shell
$ sudo docker login docker.io
username:... (rejux here)
password:...
```

push the new tag: 

```shell
$ sudo docker push rejux/vim-dev-lab:2.0.0
```

## 5. Check on the docker hub

Connect to the site and check

Search all the images with rejux:

```shell
$ sudo docker search rejux
NAME               	DESCRIPTION
rejux/vim-dev-lab  	... 
```

## 6. Using the image

The best way to use these images is to clone this repository and proceed from the Section 2 above. Then one can find a mini manual - provided by the build script - to use the CPU or GPU image.

However, since these images are available on the docker hub, one can directly pull them and execute the steps described in the mini manual above. 

```shell
$ sudo docker pull rejux/vim-dev-lab:2.0.0
```
Now confer to the aforementionned mini manual to boostrap the container.

### 6.1. Using the run scripts

A set of run scripts are provided to operates the images. They serve two objectives:

1. operate these images, and
2. operate all images that inherits from them

The scripts are described here-after:

```
scripts/
├── backup-vim-dev-lab-cde.sh
├── backup-vim-dev-lab-gpu-cde.sh
├── docker-build-vim-dev-lab-gpu.sh
├── docker-build-vim-dev-lab.sh
├── git-config.sh
├── run-bg-vim-dev-lab-cde.sh
├── run-bg-vim-dev-lab-gpu-cde.sh
├── run-fg-vim-dev-lab-cde.sh
└── run-fg-vim-dev-lab-gpu-cde.sh
```

#### run-bg-vim-dev-lab-[gpu-]cde.sh

This script is the best way to boostrap the CDE. Indeed, it will launch a container is backgroung or daemon mode. Then, the user can attach one or several terminals to it and starts working. A small manual is available by passing the *-h/--help* argument to the script. 

For instance, start the 2.0.3 version of the image:

```shell
$ sudo ./run-bg-vim-dev-lab-cde.sh -v 2.0.3
[INFO]
[INFO] ## ============================================== ##
[INFO] ## run-bg-vim-dev-lab-cde.sh
[INFO] ## ============================================== ##
[INFO]
[INFO] PROJECT_NAME = vim-dev-lab
[INFO] IMAGE_NAME = vim-dev-lab
[INFO] IMAGE_VERSION = 2.0.3
[INFO] HOST_PROJECT_DIR = /home/kla/workspace/rkl-devops/vim-dev-lab/scripts/..
[INFO] CONTAINER_PROJECT_DIR = /home/developer/workspace/vim-dev-lab

[INPUT] Do you agree with these values (Y/n)?
...
[INFO] Access to the container through a terminal using the command:
[INFO] $ sudo docker exec -it vim-dev-lab /bin/bash
[INFO]
[INFO] At the end of the sseion, if necessary commit the container to a new image tag:
[INFO] $ sudo docker commit vim-dev-lab rkl-cdes/vim-mobile-web-fullstack-fe-dev-lab/vim-dev-lab:<NEW VERSION> 
[INFO]
[INFO] Once commit, one can stop the container as follows:
[INFO] $ sudo docker stop vim-dev-lab
...
```

A small user guide is displayed. The container is running as a daemon, and one can use the displayed commands to attach a terminal to it. 

**Commit the modified container:**

At the end of your session, there might be some updates made on the running container (e.g. for instance, you may have installed some new packages or equivalent); in order to persist these updates, you must commit the container to a new image tag or version. 

Confer section [Commit the image updates](#commit-the-image-updates) for details. 

**Stop and remove the obsolete container:**

Once you have committed your modifications, you must stop the container and remove the obsolete tag(s):

```shell
$ sudo docker stop vim-dev-lab
$ sudo docker rmi vim-dev-lab:2.0.x
```

#### run-fg-vim-dev-lab-[gpu-]cde.sh

This script does quite the same thing than the preceeding one; but starts the container in foreground mode, and automatically attach a terminal to it. 

*WARNING: Be sure to update the script header in order to hard code the right image version...*

#### backup-vim-dev-lab-[gpu-]cde.sh

This script is used to export the image as a compressed archive to an external backup disk for instance. This is practical when one wants transport the image using an USB key for instance.

*WARNING: Be sure to update the script header in order to hard code the right image version...*

**Enjoy!**
 
