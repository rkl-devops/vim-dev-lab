#!/bin/bash

echo "[INFO]"
echo "[INFO] ## =================================== ##"
echo "[INFO] ## $(basename ${BASH_SOURCE[0]})"
echo "[INFO] ## =================================== ##"
echo "[INFO]"

# cde = containerized dev env
img_name="vim-dev-lab-gpu"
img_version="2.0.0"
dockerfile="Dockerfile.${img_name}"

echo "[INFO]"
echo "[INFO] Construct the ${img_name}:${img_version} image using ${dockerfile}..."
echo "[INFO]"

# place to the root dir

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null && pwd )"

cd "${DIR}"
docker build -f "dockerfiles/${dockerfile}" -t ${img_name}:${img_version} .
code=${?}

if [ ${code} == 0 ]; then

	echo "[INFO]"
	echo "[INFO] Image ${img_name}:${img_version} is successfully created. Done. "
	echo "[INFO] Cleaning the Docker registry by removing dangling images..."

	[ ! -z "$(docker ps -aqf status=exited)" ] \
		&& docker rm $(docker ps -aqf status=exited)

	[ ! -z "$(docker images -q -f dangling=true)" ] \
		&& docker rmi $(docker images -q -f dangling=true)

	echo "[INFO] Done."
	echo "[INFO]"

else
	echo "[ERROR] Something wrong. Failed with error code = ${code}. Abort!"
    exit ${code}

fi

cat << EOF
[INFO]
[INFO] You can start a container as follows:
[INFO]
[INFO] $ sudo docker run --runtime=nvidia --rm -d -it
[INFO]        [--privileged]
[INFO]        [--name PROJECT_NAME]
[INFO]        [-u developer]
[INFO]        [-v HOST_PROJECT_DIR:CONTAINER_PROJECT_DIR]
[INFO]        [-e DISPLAY=\$DISPLAY]
[INFO]        [-v /tmp/.X11-unix:/tmp/.X11-unix]
[INFO]        ${img_name}:${img_version}
[INFO]
[INFO] In the previous command, all [] are optional.
[INFO]
[INFO] Access to the container through a terminal using the command:
[INFO] $ sudo docker exec -it PROJECT_NAME /bin/bash
[INFO]
[INFO] Stop the container as follows:
[INFO] $ sudo docker stop PROJECT_NAME
[INFO]
[INFO] Or in foreground with:
[INFO] $ sudo docker run --runtime=nvidia --rm -it ${img_name}:${img_version}
[INFO]
EOF

exit 0

