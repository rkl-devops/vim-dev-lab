#!/bin/bash

echo "[INFO]"
echo "[INFO] ## ============================================== ##"
echo "[INFO] ## $(basename ${BASH_SOURCE[0]})"
echo "[INFO] ## ============================================== ##"
echo "[INFO]"

#
# globals
#

USER="developer"
PROJECT_NAME="vim-dev-lab-gpu"
IMAGE_NAME="${PROJECT_NAME}"
IMAGE_VERSION="2.0.1"
HOST_PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/.."
CONTAINER_PROJECT_DIR="/home/${USER}/workspace/${PROJECT_NAME}"

#
# main
#

echo -e "[INFO]"
echo -e "[INFO] Automatically set the PROJECT_NAME = \e[32m${PROJECT_NAME}\e[39m"
echo -e "[INFO] Automatically set the IMAGE_NAME = \e[32m${IMAGE_NAME}\e[39m"
echo -e "[INFO] Automatically set the IMAGE_VERSION = \e[32m${IMAGE_VERSION}\e[39m"
echo -e "[INFO] Automatically set the USER = \e[32m${USER}\e[39m"
echo -e "[INFO] Automatically set HOST_PROJECT_DIR = \e[32m${HOST_PROJECT_DIR}\e[39m"
echo -e "[INFO] Automatically set CONTAINER_PROJECT_DIR = \e[32m${CONTAINER_PROJECT_DIR}\e[39m"
echo -e "[INFO]"

echo -e "[INPUT] Do you agree with these values (Y/n)?"
read response

[ -z "$response" ] && response="y" || true

[ "$response" == "y" ] || {

	echo -e "[WARN]"
	echo -e "[WARN] Aborting the process. Restart with appropriate options."
	echo -e "[WARN] Bye!"

	exit 1

}

cat << EOF
[INFO]
[INFO] Boostraping a container (in foreground) from the image ${IMAGE_NAME}:${IMAGE_VERSION} using the command:
[INFO]
EOF

set -x
docker run --rm -it \
	--privileged \
	--name ${PROJECT_NAME} \
	-u ${USER} \
	-v ${HOST_PROJECT_DIR}:${CONTAINER_PROJECT_DIR} \
	-v /dev/bus/usb:/dev/bus/usb \
	-e DISPLAY=$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	${IMAGE_NAME}:${IMAGE_VERSION}
set +x

cat << EOF
[WARN]
[WARN] If you read this, then your session with the container is finished and the container has been removed."
[WARN]
[INFO] Bye.
EOF

exit 0


